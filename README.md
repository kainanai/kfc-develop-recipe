# kfc-develop-recipe

## 準備

### BitBucket

#### 登録手順

1. [BitBucket](https://bitbucket.org/product)にアクセス
1. "Get started for free"を押す
1. メアドを入力して"続行"を押す
1. "フルネーム"(適当に入れた)と"パスワード"を入力
1. reCAPTCHA して"Agree and sign up"を押す
1. メールが届くので"Verify my email address"を押す
1. "正しいアカウントを使っていますか？"と表示された場合は"Bitbucket Cloud にサインアップ"をクリック
1. ユニークなユーザー名を入れて"続行"を押す
1. "スキップ"を押す

### Sourcetree

#### インストール

1. [ここ](https://ja.atlassian.com/software/sourcetree)から Sourcetree をダウンロード
1. インストール
1. 途中ログインを要求されるので BitBucket のアカウント情報を入れる
1. "Git not found"が出るので"Download an embedded(略)"を押す
1. 途中 BitBucket Server と BitBucket を選ぶように聞かれたら、後者を選択する

#### 初期設定

1. ツール → オプション → 全般を選択
1. "フルネーム"に他人から見て良い名前を入れる(Kai Nanai など)
1. "メールアドレス"に他人から見えて良いメアドを入れる
1. 一番下の"Help improve(略)"のチェックを外す(任意)
1. ツール → オプション → 認証 → アカウント追加をクリック
1. "OAuth トークンを再読み込み"を押す
1. ウェブサイトが開くので"アクセスを許可する"を押す
1. Sourcetree に戻って"OK"を押す

### Node.js

#### インストール

1. [ここ](https://nodejs.org/ja/)から Node.js をダウンロード(LTS 版でよい)
1. インストール

## 開発

### 重要

1. Sourcetree でコミットする前に、RPG ツクール MV のプロジェクトを保存すること
1. プルしてデータが更新された場合、一度 RPG ツクール MV を再起動すること

### Sourcetree

基本的な作業の流れは以下の様になる。  
クローン → 開発作業 → ステージ → コミット → プッシュ → (プル) → 開発作業に戻る

クローン：サーバからデータを取得すること。最初に一度だけ行う。  
ステージ：変更したファイルのうち、アップロードするものを選択すること。  
コミット：ステージしたファイルの変更を適用すること。  
プッシュ：コミットしたデータをサーバにアップロードすること。  
プル：サーバから最新のデータを取得すること。
